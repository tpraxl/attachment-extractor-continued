***AttachmentExtractor Continued***

--------

### Features

* This Thunderbird Add-on is a (work in progress) fixed version of the original Add-on "AttachmentExtractor" which is no longer supported and fixed by the original author. It Extracts all attachments from selected messages and then can delete, detach and more. Unfortunately, this revised version of the original Add-on will no longer be fully functional with Thunderbird 78 and will no longer be possible as a Web-/MailExtension in the future. Perhaps there will at least be a limited version for Thunderbird 78.

### Known issues

* When detaching/deleting attachments from multiple messages, there will be no links from the stripped messages to the saved attachments. To get this working we would need a small patch in Thunderbirds core, which was/is ignored by the core developers: RFE [Bug 1578801](https://bugzilla.mozilla.org/show_bug.cgi?id=1578801).

### Installation

1. [Download AttachmentExtractor Continued from the official Thunderbird add-on page](https://addons.thunderbird.net/addon/attachmentextractor-continued/)
2. [Installing an Add-on in Thunderbird](https://support.mozilla.org/kb/installing-addon-thunderbird)


### Contribution

You are welcomed to contribute to this project by:
* adding or improving the localizations (i.e. translations) of AttachmentExtractor Continued via [Crowdin](https://crowdin.com/project/attachmentextractor-continued) (if your language is not listed, please create an [issue](https://gitlab.com/ThunderbirdMailDE/attachment-extractor-continued/issues/))
* creating [issues](https://gitlab.com/ThunderbirdMailDE/attachment-extractor-continued/issues/) about problems
* creating [issues](https://gitlab.com/ThunderbirdMailDE/attachment-extractor-continued/issues/) about possible improvements


### Coders

* Alexander Ihrig (Maintainer)
* Andrew Williamson (Original Author)

### Translators

* 


### License

[Mozilla Public License version 2.0](https://gitlab.com/ThunderbirdMailDE/attachment-extractor-continued/LICENSE)